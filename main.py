"""
CRUD es el acrónimo de "Crear, Leer, Actualizar y Borrar", 
que se usa para referirse a las funciones básicas en bases 
de datos o la capa de persistencia en un software.
"""
from Crud import Crud

crud = Crud("Localhost","transportes","postgres","admin")

#Esto se hace para mostrar los datos mejor ordenados
pasajeros = crud.leer_pasajeros()
for pasajero in pasajeros:
    print(pasajero)

id = int(input("¿Que pasajero desea eliminar? "))
crud.eliminar_pasajero(id)
pasajeros = crud.leer_pasajeros()
for pasajero in pasajeros:
    print(pasajero)

#En esta linea de comando se imprime la base de datos de pasajeros, pero desorganizado  
#print(crud.leer_pasajeros())



#capa de presentacion para ingresar datos
"""
for i in range (0,5):
    nombre= input("Ingrese nombre de pasajero: ")
    dir=input("Ingrese direccion de pasajero: ")
    tele=input("Ingrese telefono de pasajero: ")
    fecha=input("Ingrese fecha de pasajero: ")
    crud.insertar_pasajero(nombre,dir,tele,fecha)
"""

#crud.insertar_pasajero("Angela","cr 777 # 77-77","(777) 1231132", "1990-12-15")