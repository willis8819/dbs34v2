import psycopg2

class Conexion_db:

#CONSTRUCTOR
#siempre se necesita el parametro self
#los atributos siempre se declaran con self en el caso de python
    def __init__(self,mi_host,database,user,passwd):
        try:
            #fruto de la conexion se crea el objeto conn como atributo(self.conn)
            self.conn = psycopg2.connect( # aqui se convierte en un atributo
                host="localhost", #al tratarse de la db alojada en server local
                database="transportes", # nombre de la base de datos
                user="postgres", #nombre de usuario (por defecto es postgres)
                password="8128") #contraseña configurada en instalacion
            self.cur = self.conn.cursor() #Se crea el objeto cur como atributo
        except:
            print("Error en la conexion")

    def consultar_db(self,query):
        #Metodo usado para queries que retornan datos (SELECT)
        #Como parametro esta la query que se va a ejecutar (SELECT * from ...)
        try:
            self.cur.execute(query) #El objeto cur ejecuta la query
            response = self.cur.fetchall()#par leer lo que no retorna la query
            return response #devuelvo lo leido
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
    
    #ENCAPSULAR - lo que hace un insert, delete, update, en un solo metodo, solo se debe escribir db
    def escribir_db(self,query):
        #METODO USADO PARA QUERIES QUE NO RETORNAN DATOS
        #encapsular
        try:
            self.cur.execute(query) # atributo de la clase, se ejecuta
            self.conn.commit() # atributo de la clase , se actualiza la db
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
    def cerrar_db(self):
        #Cerrar conexion
        self.cur.close()
        self.conn.close()