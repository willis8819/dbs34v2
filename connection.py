import psycopg2

#el codigo para conectarme a la base de datos
#necesito crear un objeto de la clase connect y enviar las credenciales de la base de datos

conn = psycopg2.connect(
    host="localhost", #al tratarse de la db alojada en server local
    database="transportes", # nombre de la base de datos
    user="postgres", #nombre de usuario (por defecto es postgres)
    password="8128", #contraseña configurada en instalacion
)

#se necesita crea un objeto de la clase cursor
cur = conn.cursor() # mediante este objeto cur, puedo poder ingresar o ejecutar querys
#traer la version de postgres instalada
"""
query = "SELECT version()" #probar que funciona
cur.execute(query) # aqui se ejecuta la query
db_version=cur.fetchone() #Para leer lo que nos retorna la query y almacenarlo en la varible db_version
print(db_version[0])
"""
#Lo anterior es la plantilla base 

#METODO 1 TRAYENDO TODOS LOS DATOS
"""
query ="SELECT * from \"Pasajeros\";" #Traer los registro de la tabla Pasajeros
#print(query)
cur.execute(query)
response = cur.fetchall()# Para retornar lo que nos retorna la query
print(response) 
for reg in response:
    nombre=reg[1]
    dir=reg[2]
    tel=reg[3]
    fecha=reg[4]
    print("Nombre ",nombre," dir: ",dir," tel: ",tel," fecha: ",fecha)
"""

"""
#METODO 2 TRAYENDO SOLO LOS DATOS NECESARIOS
query ="SELECT \"nombre\",\"direccion\","+\
    "\"telefono\",\"fecha_nacimiento\""+\
        " from \"Pasajeros\";"#Traer los registros de la tabla Pasajeros
cur.execute(query)
response = cur.fetchall()# Para retornar lo que nos retorna la query 
for reg in response:
    nombre=reg[0]
    dir=reg[1]
    tel=reg[2]
    fecha=reg[3]
    print("Nombre ",nombre," dir: ",dir," tel: ",tel," fecha: ",fecha)

"""
"""
#PARA AGREGAR DATOS
try:
    query= "INSERT INTO \"Pasajeros\""+\
        "(nombre, direccion, telefono, fecha_nacimiento)"+\
        "VALUES ('Andres','C3 123123', '(123) 1231231', '2000-11-11')"
    cur.execute(query)
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    print(error)
"""

"""
#PARA ELIMINAR DATOS
try:
    query= "DELETE FROM \"Pasajeros\""+\
        "WHERE id=53"
    cur.execute(query)
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    print(error)

"""

#PARA ACTUALIZAR DATOS
try:
    query= "UPDATE  \"Pasajeros\""+\
        "SET nombre='ANTONIO' WHERE id =40 "
    cur.execute(query)
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    print(error)

