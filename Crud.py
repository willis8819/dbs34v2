from Conexion_db import Conexion_db # inicializar la conexion
class Crud: #se define la clase
    #Esta es la clase encargada de contener las queries
    #y ejecutarlas a traves de la clase conexion_db
    def __init__(self,mi_host,database,user,passwd): #constructor
        self.conn = Conexion_db(mi_host,database,user,passwd) #se crea el objeto conn como atributo
        #el objeto conn es un objeto de la clase Conexion_db y un atributo de la clase

#Acá se definen los metodos para escribir, leer, actualizar tablas

# primer metodo del crud
    def insertar_pasajero (self,nombre, direccion, telefono, fecha_nacimiento):
        query= "INSERT INTO \"Pasajeros\""+\
        "(nombre, direccion, telefono, fecha_nacimiento)"+\
            "VALUES ('"+nombre+"','"+direccion+\
                "','"+telefono+"','"+fecha_nacimiento+"')"
        self.conn.escribir_db(query)

    def leer_pasajeros(self):
        query = "SELECT * from \"Pasajeros\""
        pasajeros = self.conn.consultar_db(query)
        return pasajeros

    def eliminar_pasajero(self,id):
        query = "DELETE FROM \"Pasajeros\""+\
            "WHERE id = "+str(id)
        self.conn.escribir_db(query)

